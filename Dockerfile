FROM node:16.10.0-alpine AS node

WORKDIR /app

COPY package*.json ./

RUN npm ci

COPY . .

CMD ["npm", "run", "start:dev"]
