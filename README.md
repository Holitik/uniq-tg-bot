## Installation

copy .env.example as .env.development

```bash
# build
$ docker-compose build
```

## Running the app

```bash

# dev mode
$ docker-compose up
```
