import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToInstance } from 'class-transformer';
import { UploadFileDto } from '../dto/uploadFile.dto';

@Injectable()
export class UploadFileDtoValidationPipe implements PipeTransform {
  async transform(file: UploadFileDto) {
    if (!file) {
      throw new BadRequestException(
        `Ошибка валидации. На вход ожидается класс UploadFileDto`,
      );
    }
    const myDtoObject = plainToInstance(UploadFileDto, file);
    const errors = await validate(myDtoObject);
    if (errors.length > 0) {
      const message = errors.map((error) => {
        return `${error.property} - ${Object.values(error.constraints).join(
          ', ',
        )}`;
      });
      throw new BadRequestException(
        `Ошибка валидации. UploadFileDto не прошел валидацию 
        ${message}`,
      );
    }
    return file;
  }
}
