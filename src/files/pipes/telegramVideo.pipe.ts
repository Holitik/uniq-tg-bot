import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { Context } from 'telegraf';

@Injectable()
export class TelegramVideoValidationPipe implements PipeTransform {
  async transform(ctx: Context) {
    const arg = 'video';
    if (!(arg in ctx.message)) {
      throw new BadRequestException(
        `В контексте сообщения телеграм нет поля ${arg}`,
      );
    }
    return ctx;
  }
}
