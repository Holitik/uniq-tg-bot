import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToInstance } from 'class-transformer';
import { FileDto } from '../dto/file.dto';

@Injectable()
export class FileDtoValidationPipe implements PipeTransform {
  async transform(file: FileDto) {
    if (!file) {
      throw new BadRequestException(
        `Ошибка валидации. На вход ожидается класс FileDto`,
      );
    }
    const myDtoObject = plainToInstance(FileDto, file);
    const errors = await validate(myDtoObject);
    if (errors.length > 0) {
      const message = errors.map((error) => {
        return `${error.property} - ${Object.values(error.constraints).join(
          ', ',
        )}`;
      });
      console.log(errors);
      throw new BadRequestException(
        `Ошибка валидации. FileDto не прошел валидацию 
        ${message}`,
      );
    }
    return file;
  }
}
