import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { Context } from 'telegraf';

@Injectable()
export class TelegramDocumentValidationPipe implements PipeTransform {
  async transform(ctx: Context) {
    const arg = 'document';
    if (!(arg in ctx.message)) {
      throw new BadRequestException(
        `В контексте сообщения телеграм нет поля ${arg}`,
      );
    }
    return ctx;
  }
}
