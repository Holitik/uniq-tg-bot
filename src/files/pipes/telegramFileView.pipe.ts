import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToInstance } from 'class-transformer';
import { TelegramFileView } from '../dto/telegramFileView';
import { ValidationException } from '../exceptions/validationException';

@Injectable()
export class TelegramFileValidationPipe implements PipeTransform {
  async transform(file: TelegramFileView) {
    if (!file) {
      throw new BadRequestException(
        `Ошибка валидации. На вход ожидается класс TelegramFileView`,
      );
    }
    const myDtoObject = plainToInstance(TelegramFileView, file);
    const errors = await validate(myDtoObject);
    if (errors.length > 0) {
      const message = errors.map((error) => {
        return `${error.property} - ${Object.values(error.constraints).join(
          ', ',
        )}`;
      });
      const errorTypes = errors.map((error) => {
        return error.property;
      });
      throw new ValidationException(
        `Ошибка валидации. TelegramFileView не прошел валидацию 
        ${message}`,
        errorTypes,
      );
    }
    return file;
  }
}
