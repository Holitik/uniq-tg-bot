import { BadRequestException } from '@nestjs/common/exceptions';
export class ValidationException extends BadRequestException {
  errorsList: string[];
  devMessage: string;
  constructor(devMessage: string, errorsList: string[]) {
    super(errorsList, devMessage);
    this.errorsList = errorsList;
    this.devMessage = devMessage;
  }
}
