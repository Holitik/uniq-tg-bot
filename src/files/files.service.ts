import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { map, lastValueFrom } from 'rxjs';
import axios from 'axios';
import { AxiosResponse } from 'axios';
import { FileDto } from './dto/file.dto';
import * as FormData from 'form-data';
import * as AdmZip from 'adm-zip';
import { UploadFileDto } from './dto/uploadFile.dto';
import { BadRequestException } from '@nestjs/common/exceptions';

@Injectable()
export class FilesService {
  constructor(private readonly httpService: HttpService) {}
  private readonly logger = new Logger(FilesService.name);

  async getFilesList(): Promise<FileDto[]> {
    const url = '/list';
    let filesList: FileDto[];
    try {
      filesList = await lastValueFrom(
        this.httpService.get(url).pipe(
          map((res: AxiosResponse) => {
            if (res.data) return res.data;
            else return [];
          }),
        ),
      );
    } catch (error) {
      throw new BadRequestException('Не удалось получить данные');
    }

    return filesList;
  }

  private unzipBufferResponse(buffer: Buffer): Buffer[] {
    const buffers: Buffer[] = [];
    const zip = new AdmZip(buffer);
    if (!zip) return buffers;
    const zipEntries = zip.getEntries();
    if (!zipEntries) return buffers;
    zipEntries.forEach((entry) => {
      buffers.push(entry.getData());
    });
    return buffers;
  }

  async fileDownloadById(fileId: string): Promise<Buffer[]> {
    const url = `/download/${fileId}`;
    const listBuffers = await lastValueFrom(
      this.httpService.post(url, {}, { responseType: 'arraybuffer' }).pipe(
        map((res: AxiosResponse) => {
          if (res.status === 200 || res.status === 201) {
            return this.unzipBufferResponse(res.data);
          } else {
            throw new BadRequestException('Не удалось скачать файл с uniq api');
          }
        }),
      ),
    );

    return listBuffers;
  }
  async uploadFile(uploadFilesDto: UploadFileDto): Promise<FileDto> {
    const fileBuffer: ArrayBuffer = (
      await axios
        .get(uploadFilesDto.telegramUrl, {
          responseType: 'arraybuffer',
        })
        .catch((error) => {
          this.logger.error(error.message);
          throw new BadRequestException(
            'Не удалось загрузить файл из телеграмма как буфер',
          );
        })
    ).data;

    const bodyFormData = new FormData();
    bodyFormData.append('file', fileBuffer, uploadFilesDto.fileName);
    bodyFormData.append('copies', uploadFilesDto.copies);
    const uniqUrl = `/upload/${uploadFilesDto.fileType}`;
    const result = await lastValueFrom(
      this.httpService
        .post(uniqUrl, bodyFormData, {
          headers: {
            'Content-Type': 'multipart/form-data',
            ...bodyFormData.getHeaders(),
          },
        })
        .pipe(
          map((res: AxiosResponse) => {
            if (res.status === 200 || res.status === 201) {
              const file: FileDto = res.data;
              file.telegramMessageId = uploadFilesDto.telegramMessageId;
              return file;
            } else {
              throw new BadRequestException(
                'Не удалось загрузить файл на uniq api',
              );
            }
          }),
        ),
    );

    return result;
  }
}
