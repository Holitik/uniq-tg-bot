import {
  IsNotEmpty,
  IsString,
  IsNumber,
  IsPositive,
  Max,
  IsEnum,
  IsUrl,
  IsInt,
} from 'class-validator';
import { TypeEnum } from '../enum/type.enum';

export class UploadFileDto {
  @IsString({ message: 'Поле не строка' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly fileName: string;

  @IsString({ message: 'Поле не строка' })
  @IsUrl({}, { message: 'Поле не ссылка' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly telegramUrl: string;

  @IsEnum(TypeEnum, { message: 'Поле не относиться к типу TypeEnum' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly fileType: TypeEnum;

  @IsInt({ message: 'Поле не число' })
  @IsPositive({ message: 'Значение не может быть отрицательным' })
  @Max(50, { message: 'Значение вышло за границы' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly copies: number;

  @IsNumber({}, { message: 'Поле не число' })
  @IsPositive({ message: 'Значение не может быть отрицательным' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly telegramMessageId: number;
}
