import {
  IsString,
  IsEnum,
  Max,
  IsNotEmpty,
  IsInt,
  IsPositive,
  IsNumber,
} from 'class-validator';
import { StatusEnum } from '../enum/status.enum';
import { TypeEnum } from '../enum/type.enum';

export class FileDto {
  @IsString({ message: 'Поле не строка' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly id: string;

  @IsString({ message: 'Поле не строка' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly file: string;

  @IsInt({ message: 'Поле не число' })
  @IsPositive({ message: 'Значение не может быть отрицательным' })
  @Max(50, { message: 'Значение вышло за границы' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly copies: number;

  readonly createdAt: Date;

  @IsEnum(StatusEnum, { message: 'Поле не относиться к типу StatusEnum' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly status: StatusEnum;

  @IsEnum(TypeEnum, { message: 'Поле не относиться к типу TypeEnum' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly type: TypeEnum;

  @IsNumber({}, { message: 'Поле не число' })
  @IsNotEmpty({ message: 'Поле пустое' })
  telegramMessageId: number;
}
