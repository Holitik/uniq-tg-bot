import {
  IsEnum,
  IsString,
  Max,
  IsNotEmpty,
  IsInt,
  IsPositive,
} from 'class-validator';
import { MimeTypeEnum } from '../enum/mimeType';
export class TelegramFileView {
  @IsString({ message: 'Поле не строка' })
  @IsNotEmpty({ message: 'Поле пустое' })
  file_id: string;

  @IsEnum(MimeTypeEnum, { message: 'Поле не относиться к типу MimeTypeEnum' })
  @IsNotEmpty({ message: 'Поле пустое' })
  mime_type: MimeTypeEnum;

  @IsString({ message: 'Поле не строка' })
  @IsNotEmpty({ message: 'Поле пустое' })
  file_name: string;

  @IsInt({ message: 'Поле не число' })
  @IsPositive({ message: 'Значение не может быть отрицательным' })
  @Max(52428800, { message: 'Значение вышло за границы' })
  @IsNotEmpty({ message: 'Поле пустое' })
  file_size: number;
}
