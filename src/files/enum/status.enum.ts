export enum StatusEnum {
  Pending = 'pending',
  InWork = 'inWork',
  Done = 'done',
  Error = 'error',
  Canceled = 'canceled',
}
