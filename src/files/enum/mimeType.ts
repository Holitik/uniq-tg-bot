export enum MimeTypeEnum {
  Png = 'image/png',
  Jpeg = 'image/jpeg',
  Jpg = 'image/jpg',
  Mp4 = 'video/mp4',
}
