export interface ISessionData {
  copies: number;
  isInputDefaultCopies: boolean;
}
