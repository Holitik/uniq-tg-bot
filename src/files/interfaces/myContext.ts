import { Context } from 'telegraf';
import { ISessionData } from './sessionData';

export interface MyContext extends Context {
  session?: ISessionData;
}
