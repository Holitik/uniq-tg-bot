import { HttpModule } from '@nestjs/axios';
import { forwardRef, Module } from '@nestjs/common';
import { FilesService } from './files.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CronModule } from '../cron/cron.module';

@Module({
  imports: [
    HttpModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        timeout: configService.get('HTTP_TIMEOUT'),
        maxRedirects: configService.get('HTTP_MAX_REDIRECTS'),
        baseURL: configService.get('UNIQ_API_BASE_URL'),
        auth: {
          username: configService.get('UNIQ_API_LOGIN'),
          password: configService.get('UNIQ_API_PASSWORD'),
        },
      }),
      inject: [ConfigService],
    }),
    forwardRef(() => CronModule),
  ],
  exports: [FilesService],
  providers: [FilesService],
})
export class FilesModule {}
