import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateCronItemDto {
  @IsNumber({}, { message: 'Поле не число' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly chatId: number;

  @IsNumber({}, { message: 'Поле не число' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly telegramMessageId: number;

  @IsString({ message: 'Поле не строка' })
  @IsNotEmpty({ message: 'Поле пустое' })
  readonly fileId: string;
}
