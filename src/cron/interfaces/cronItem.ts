export interface ICronItem {
  chatId: number;
  telegramMessageId: number;
  fileId: string;
}
