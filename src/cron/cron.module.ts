import { forwardRef, Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { SequelizeModule } from '@nestjs/sequelize';
import { CronService } from './cron.service';
import { CronItem } from './cronItem.model';
import { FilesModule } from '../files/files.module';

@Module({
  providers: [CronService],
  imports: [
    SequelizeModule.forFeature([CronItem]),
    ScheduleModule.forRoot(),
    forwardRef(() => FilesModule),
  ],
  exports: [CronService],
})
export class CronModule {}
