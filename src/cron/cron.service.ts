import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { InjectModel } from '@nestjs/sequelize';
import { InjectBot } from 'nestjs-telegraf';
import { Context, Telegraf } from 'telegraf';
import { CronItem } from './cronItem.model';
import { CreateCronItemDto } from './dto/createCronItem.dto';
import { FilesService } from '../files/files.service';
import { StatusEnum } from 'src/files/enum/status.enum';
import { TypeEnum } from 'src/files/enum/type.enum';
import { ICronItem } from './interfaces/cronItem';
import { BadRequestException } from '@nestjs/common/exceptions';
import { FileDto } from '../files/dto/file.dto';

@Injectable()
export class CronService {
  constructor(
    @InjectModel(CronItem) private cronRepository: typeof CronItem,
    @InjectBot() private bot: Telegraf<Context>,
    private readonly filesService: FilesService,
  ) {}
  private readonly logger = new Logger(CronService.name);

  async addCronItem(dto: CreateCronItemDto): Promise<void> {
    const searchItem = await this.cronRepository.findByPk(dto.fileId);
    if (searchItem)
      throw new BadRequestException('Файл c таким id уже есть в базе');
    try {
      await this.cronRepository.create(dto);
    } catch (error) {
      throw new BadRequestException('Не удалось сохранить файл в базу');
    }
  }

  async getCronItemsList(): Promise<ICronItem[]> {
    const searchItem = await this.cronRepository.findAll();
    if (!searchItem) return Array(0);
    return searchItem;
  }

  async removeCronItemByFileId(fileId: string): Promise<void> {
    const searchItem = await this.cronRepository.findByPk(fileId);
    if (!searchItem) return null;
    try {
      await searchItem.destroy();
    } catch (error) {
      throw new BadRequestException(
        'Не удалось удалить элемент из базы данных',
      );
    }
  }

  @Cron('*/10 * * * * *')
  async cronCheckAndSendFiles() {
    const cronList = await this.getCronItemsList();
    if (cronList.length <= 0) return;

    let uniqFilesList: FileDto[];
    try {
      uniqFilesList = await this.filesService.getFilesList();
    } catch (error) {
      this.logger.error(error.message);
    }
    if (uniqFilesList.length <= 0) return;

    for (const item of cronList) {
      const file = uniqFilesList.find((i) => {
        if (i.id == item.fileId && i.status === StatusEnum.Done) {
          return i;
        }
      });
      if (!file) return;

      const filesBuffers = await this.filesService
        .fileDownloadById(file.id)
        .catch((error) => {
          this.logger.error(error.message);
        });
      if (!filesBuffers) return;

      for (const buffer of filesBuffers) {
        try {
          if (file.type === TypeEnum.Photo) {
            await this.bot.telegram.sendPhoto(
              item.chatId,
              {
                source: buffer,
              },
              {
                reply_to_message_id: item.telegramMessageId,
                caption: `Файл готов`,
              },
            );
          }
          if (file.type === TypeEnum.Video) {
            await this.bot.telegram.sendVideo(
              item.chatId,
              {
                source: buffer,
              },
              { reply_to_message_id: item.telegramMessageId },
            );
          }
        } catch (error) {
          this.logger.error(error.message);
        }
      }
      await this.removeCronItemByFileId(file.id).catch((error) => {
        this.logger.error(error.message);
      });
    }
  }
}
