import { Column, DataType, Model, Table } from 'sequelize-typescript';

interface CronItemCreationAttributes {
  fileId: string;
  chatId: number;
  telegramId: number;
}

@Table({ tableName: 'cronItem', createdAt: false, updatedAt: false })
export class CronItem extends Model<CronItem, CronItemCreationAttributes> {
  @Column({
    type: DataType.STRING,
    unique: true,
    primaryKey: true,
  })
  fileId: string;

  @Column({
    type: DataType.BIGINT,
    allowNull: true,
  })
  chatId: number;

  @Column({
    type: DataType.BIGINT,
    allowNull: true,
  })
  telegramMessageId: number;
}
