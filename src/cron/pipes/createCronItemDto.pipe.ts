import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToInstance } from 'class-transformer';
import { CreateCronItemDto } from '../dto/createCronItem.dto';

@Injectable()
export class CreateCronItemDtoValidationPipe implements PipeTransform {
  async transform(item: CreateCronItemDto) {
    if (!item) {
      throw new BadRequestException(
        `Ошибка валидации. На вход ожидается класс CreateCronItemDto`,
      );
    }
    const myDtoObject = plainToInstance(CreateCronItemDto, item);
    const errors = await validate(myDtoObject);
    if (errors.length > 0) {
      const message = errors.map((error) => {
        return `${error.property} - ${Object.values(error.constraints).join(
          ', ',
        )}`;
      });
      throw new BadRequestException(
        `Ошибка валидации. CreateCronItemDto не прошел валидацию 
        ${message}`,
      );
    }
    return item;
  }
}
