import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MyLogger } from './logger/logger.service';

async function bootstrap() {
  process.on('unhandledRejection', (error: any) => {
    logger.error('Упс, необработанное исключение', error.message);
  });

  const port = process.env.PORT;

  const app = await NestFactory.create(AppModule, { bufferLogs: true });

  app.useLogger(app.get(MyLogger));

  const logger = new Logger();

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      forbidUnknownValues: true,
      disableErrorMessages: false,
      validationError: {
        value: false,
      },
      transform: true,
    }),
  );

  await app.listen(port, () => {
    logger.warn(`Server started on port : ${port}`);
  });
}
bootstrap();
