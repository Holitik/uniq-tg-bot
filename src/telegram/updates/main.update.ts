import { Ctx, Start, Update } from 'nestjs-telegraf';
import { ReplyKeyboardMarkup } from 'telegraf/typings/core/types/typegram';
import { Markup } from 'telegraf';
import { MyContext } from 'src/files/interfaces/myContext';

@Update()
export class MainUpdate {
  replyChangeCountCopiesKeyboard: ReplyKeyboardMarkup = Markup.keyboard([
    ['Изменить счетчик копий'],
  ]).resize(true).reply_markup;

  private init(@Ctx() ctx: MyContext) {
    ctx.session.copies = 1;
    ctx.session.isInputDefaultCopies = false;
  }

  @Start()
  async start(@Ctx() ctx: MyContext) {
    this.init(ctx);
    ctx.reply(
      'Привет, это uniq бот. Загрузи в него изображение или видео и мы уникализируем его',
      { reply_markup: this.replyChangeCountCopiesKeyboard },
    );
  }
}
