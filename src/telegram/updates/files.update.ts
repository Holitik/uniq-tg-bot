import { Ctx, Hears, On, Update } from 'nestjs-telegraf';
import { BadRequestException } from '@nestjs/common/exceptions';
import { Logger, UsePipes, ValidationPipe } from '@nestjs/common';
import { CreateCronItemDto } from 'src/cron/dto/createCronItem.dto';
import { FilesService } from 'src/files/files.service';
import { CronService } from 'src/cron/cron.service';
import { MimeTypeEnum } from 'src/files/enum/mimeType';
import { MyContext } from 'src/files/interfaces/myContext';
import { TelegramFileView } from 'src/files/dto/telegramFileView';
import { TelegramFileValidationPipe } from 'src/files/pipes/telegramFileView.pipe';
import { TypeEnum } from 'src/files/enum/type.enum';
import { UploadFileDto } from 'src/files/dto/uploadFile.dto';
import { TelegramPhotoValidationPipe } from 'src/files/pipes/telegramPhoto.pipe';
import { TelegramDocumentValidationPipe } from 'src/files/pipes/telegramDocument.pipe';
import { TelegramVideoValidationPipe } from 'src/files/pipes/telegramVideo.pipe';
import { UploadFileDtoValidationPipe } from 'src/files/pipes/uploadFileDto.pipe';
import { FileDtoValidationPipe } from 'src/files/pipes/fileDto.pipe';
import { FileDto } from '../../files/dto/file.dto';
import { CreateCronItemDtoValidationPipe } from 'src/cron/pipes/createCronItemDto.pipe';
import { File } from 'telegraf/typings/core/types/typegram';

@Update()
@UsePipes(new ValidationPipe())
export class FilesUpdate {
  constructor(
    private readonly filesService: FilesService,
    private readonly cronService: CronService,
  ) {}
  private readonly logger = new Logger(FilesUpdate.name);
  private photoTypes = [MimeTypeEnum.Png, MimeTypeEnum.Jpeg, MimeTypeEnum.Jpg];
  private videoTypes = [MimeTypeEnum.Mp4];
  private defaultCopies = 1;
  private minCopies = 1;
  private maxCopies = 50;

  private async loadFile(
    ctx: MyContext,
    file: TelegramFileView,
  ): Promise<void> {
    const messageId = ctx.message.message_id;
    const validatedFile = await new TelegramFileValidationPipe()
      .transform(file)
      .catch((error) => {
        error.errorsList.forEach((err: string) => {
          if (err === 'file_size') {
            ctx.reply(`Файл слишком большой`, {
              reply_to_message_id: messageId,
            });
          }
          if (err === 'mime_type') {
            ctx.reply(`Файл имеет неподдерживаемый формат`, {
              reply_to_message_id: messageId,
            });
          }
        });
        throw new BadRequestException(error.devMessage);
      });
    let type: TypeEnum;
    if (this.photoTypes.includes(validatedFile.mime_type)) {
      type = TypeEnum.Photo;
    }
    if (this.videoTypes.includes(validatedFile.mime_type)) {
      type = TypeEnum.Video;
    }

    let validateFileDto: FileDto;
    try {
      let url: string;
      try {
        url = (await ctx.telegram.getFileLink(validatedFile.file_id)).href;
      } catch (error) {
        throw new BadRequestException(
          'Не удалось получить ссылку на телеграм файл',
        );
      }
      const uploadFilesDto: UploadFileDto = {
        fileName: validatedFile.file_name,
        telegramUrl: url,
        fileType: type,
        telegramMessageId: messageId,
        copies: ctx.session.copies ? ctx.session.copies : this.defaultCopies,
      };
      const validatedUploadFilesDto =
        await new UploadFileDtoValidationPipe().transform(uploadFilesDto);

      const fileDto = await this.filesService
        .uploadFile(validatedUploadFilesDto)
        .catch((error) => {
          throw new BadRequestException(error.message);
        });
      validateFileDto = await new FileDtoValidationPipe().transform(fileDto);
    } catch (error) {
      await ctx.reply('Не удалось загрузить файл', {
        reply_to_message_id: messageId,
      });
      throw new BadRequestException(error.message);
    }
    const item: CreateCronItemDto = {
      chatId: ctx.message.chat.id,
      telegramMessageId: validateFileDto.telegramMessageId,
      fileId: validateFileDto.id,
    };
    const validatedItem = await new CreateCronItemDtoValidationPipe().transform(
      item,
    );

    await this.cronService.addCronItem(validatedItem).catch((error) => {
      ctx.reply('Проблемы с сервером, повторите попытку', {
        reply_to_message_id: messageId,
      });
      throw new BadRequestException(error.message);
    });

    await ctx.reply(
      `Файл ${validateFileDto.file} успешно загружен и поставлен в очередь на обработку, копий : ${validateFileDto.copies}`,
      {
        reply_to_message_id: messageId,
      },
    );
  }

  @UsePipes(TelegramPhotoValidationPipe)
  @On('photo')
  async photoLoadFile(@Ctx() ctx: MyContext) {
    let file: TelegramFileView;
    let fileData: File;

    try {
      file = ctx.message['photo'].pop();
      fileData = await ctx.telegram.getFile(file.file_id);
    } catch (error) {
      ctx.reply('Не удалось получить ваш файл, повторите попытку', {
        reply_to_message_id: ctx.message.message_id,
      });
      this.logger.error(error.message);
      return;
    }

    const fileName = fileData.file_path.split('/')[1];
    file.file_name = fileName;
    file.file_size = fileData.file_size;
    file.mime_type = MimeTypeEnum.Jpeg;

    try {
      await this.loadFile(ctx, file).catch((error) => {
        this.logger.error(error.message);
      });
    } catch (error) {
      this.logger.error(error);
    }
  }

  @UsePipes(TelegramVideoValidationPipe)
  @On('video')
  async videoLoadFiles(@Ctx() ctx: MyContext) {
    const file: TelegramFileView = ctx.message['video'];
    if (!file) {
      ctx.reply('Не удалось получить ваш файл, повторите попытку', {
        reply_to_message_id: ctx.message.message_id,
      });
      return;
    }
    try {
      await this.loadFile(ctx, file);
    } catch (error) {
      this.logger.error(error.message);
    }
  }

  @UsePipes(TelegramDocumentValidationPipe)
  @On('document')
  async documentLoadFiles(@Ctx() ctx: MyContext) {
    const file: TelegramFileView = ctx.message['document'];
    if (!file) {
      ctx.reply('Не удалось получить ваш файл, повторите попытку', {
        reply_to_message_id: ctx.message.message_id,
      });
      return;
    }
    try {
      await this.loadFile(ctx, file);
    } catch (error) {
      this.logger.error(error.message);
    }
  }

  @Hears('Изменить счетчик копий')
  async changeCountCopies(@Ctx() ctx: MyContext) {
    await ctx.reply(
      `Введите число копий, которое будет стоять по уполчанию(от ${this.minCopies} до ${this.maxCopies})`,
    );
    ctx.session.isInputDefaultCopies = true;
  }

  @On('text')
  async text(@Ctx() ctx: MyContext) {
    if (ctx.session.isInputDefaultCopies) {
      const num: number = ctx.message['text'];
      if (!isNaN(num) && num >= this.minCopies && num <= this.maxCopies) {
        ctx.session.copies = Number(num);
        ctx.reply(
          `Количество копий по умолчанию изменено на : ${ctx.session.copies}`,
        );
        ctx.session.isInputDefaultCopies = false;
      } else {
        ctx.reply(
          `Введите число. Число копий может быть от ${this.minCopies} до ${this.maxCopies}`,
        );
        return;
      }
    }
  }
}
