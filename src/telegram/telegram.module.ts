import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TelegrafModule } from 'nestjs-telegraf';
import { session } from 'telegraf';
import { FilesUpdate } from './updates/files.update';
import { MainUpdate } from './updates/main.update';
import { FilesModule } from '../files/files.module';
import { CronModule } from '../cron/cron.module';
@Module({
  imports: [
    TelegrafModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        token: configService.get<string>('BOT_TOKEN'),
        middlewares: [session()],
      }),
      inject: [ConfigService],
    }),
    FilesModule,
    CronModule,
  ],
  providers: [MainUpdate, FilesUpdate],
})
export class TelegramModule {}
