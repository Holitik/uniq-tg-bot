import { IsEnum, IsNumber, IsString, IsUrl, Min, Max } from 'class-validator';

enum Environment {
  Development = 'development',
  Production = 'production',
}

export class AppEnvironmentVariables {
  @IsEnum(Environment)
  NODE_ENV: Environment;

  @IsNumber()
  @Min(0)
  @Max(65535)
  PORT: number;

  @IsUrl()
  UNIQ_API_BASE_URL: string;

  @IsString()
  UNIQ_API_LOGIN: string;

  @IsString()
  UNIQ_API_PASSWORD: string;

  @IsString()
  BOT_TOKEN: string;

  @IsNumber()
  HTTP_TIMEOUT: number;

  @IsNumber()
  HTTP_MAX_REDIRECTS: number;

  @IsString()
  POSTGRES_HOST: string;

  @IsNumber()
  @Min(0)
  @Max(65535)
  POSTGRES_PORT: number;

  @IsString()
  POSTGRES_USER: string;

  @IsString()
  POSTGRES_DB: string;

  @IsString()
  POSTGRES_PASSWORD: string;
}
