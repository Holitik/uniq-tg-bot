import { registerAs } from '@nestjs/config';
import { IAppConfig } from './interface';
import { AppEnvironmentVariables } from './validation';
import { validateUtil } from './validateUtil';

export default registerAs('app', (): IAppConfig => {
  validateUtil(process.env, AppEnvironmentVariables);

  return {
    nodeEnv: process.env.NODE_ENV,
    port: parseInt(process.env.PORT),
    uniqApiBaseUrl: process.env.UNIQ_API_BASE_URL,
    uniqApiLogin: process.env.UNIQ_API_LOGIN,
    uniqApiPassword: process.env.UNIQ_API_PASSWORD,
    botToken: process.env.BOT_TOKEN,
    httpTimeout: parseInt(process.env.HTTP_TIMEOUT),
    httpMaxRedirects: parseInt(process.env.HTTP_MAX_REDIRECTS),
    PostgresHost: process.env.POSTGRES_HOST,
    PostgresPort: parseInt(process.env.POSTGRES_PORT),
    PostgresUser: process.env.POSTGRES_USER,
    PostgresDB: process.env.POSTGRES_DB,
    PostgresPassword: process.env.POSTGRES_PASSWORD,
  };
});
