export interface IAppConfig {
  nodeEnv: string;
  port: number;
  uniqApiBaseUrl: string;
  uniqApiLogin: string;
  uniqApiPassword: string;
  botToken: string;
  httpTimeout: number;
  httpMaxRedirects: number;
  PostgresHost: string;
  PostgresPort: number;
  PostgresUser: string;
  PostgresDB: string;
  PostgresPassword: string;
}
